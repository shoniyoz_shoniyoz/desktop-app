import { createRouter, createWebHistory } from "vue-router";

// function authGuard(to: any, from: any, next: any) {
//   if (localStorage.getItem("token")) {
//     next();
//   } else {
//     next("/login");
//   }
// }

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    // {
    //   path: "/login",
    //   name: "login",
    //   meta: { layout: "default" },
    //   component: () => import("../pages/login/LIndex.vue"),
    // },
    {
      path: "/",
      name: "home",
      meta: { layout: "main" },
      component: () => import("../pages/home/HIndex.vue"),
      // beforeEnter: authGuard,
    },

  ],
});

export default router;
