
export const getItem = (key: any) => {
  try {
    return JSON.parse(localStorage.getItem(key) as string);
  } catch (error) {
    console.log("Error getting data");
    return null;
  }
};

export const setItem = (key: any, data: any) => {
  try {
    localStorage.setItem(key, data);
  } catch (error) {
    console.log("Error saving data");
  }
};

export const removeItem = (key: any) => {
  try {
    localStorage.removeItem(key);
  } catch (error) {
    console.log("Error deleting data");
  }
};
